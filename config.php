<?php

	global $CONFIG_PHP;
	if(!isset($CONFIG_PHP)) {
		$CONFIG_PHP = 1;

		#Modify the next group as needed

		$DEBUG = $debug = 0;

		$NETSTORE_IMAGE_WIDTH = 1200;
		$NETSTORE_IMAGE_HEIGHT = 100;
		
		// Number of seconds before stats page refreshs
		$REFRESH_RATE = 5;
		// Number of hosts to display
		$TOP_RAW_STATS = 15;
		// Number of hosts to display
		$TOP_STATS = 5;
		// Number of seconds to display averages for.
		$STATS_TIME = 60;
		
		// Enter the number of T1's that NetStore is listening to.
		$NUMBER_OF_T1S = 1;
		
		$ERROREMAIL = $ADMINEMAIL1 = "oblodgett@parelli.com";

		$MYSQL_CONNECTION_INFO["netstore"]["DBHOST"] = "66.112.37.247";
		$MYSQL_CONNECTION_INFO["netstore"]["DBNAME"] = "netstore";
		$MYSQL_CONNECTION_INFO["netstore"]["DBUSER"] = "netstore";
		$MYSQL_CONNECTION_INFO["netstore"]["DBPASS"] = "scrubbed";
		$MYSQL_CONNECTION_INFO["netstore"]["DEBUG"] = $DEBUG;

		$ROOT = "/var/www/html/secure/admin/NetStoreWeb";

		#////////////////////////////////

		$CONNECTION_SPEED = $NUMBER_OF_T1S * 1544000;
		if($debug) { error_reporting(E_ALL); }
		$INCDIR = "/include";
		$INC["mysql"] = $ROOT . $INCDIR . "/func_db_mysql.php";
		$INC["netstore"] = $ROOT . $INCDIR . "/func_netstore_tbl_data.php";
		$INC["extra"] = $ROOT . $INCDIR . "/func_extra.php";
		include($INC["extra"]);

	}

?>
