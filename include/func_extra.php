<?php

	function print_header($title) {

		global $debug;
		if($debug) print "<!-- Starting print_header() -->\n";
		print_title($title);
		print "\t<body>\n";
		if($debug) print "<!-- Ending print_header() -->\n";

	}

	function print_title($pagetitle) {
		print "<html>\n\t<head>\n\t\t<title>$pagetitle</title>\n";
		print "\t</head>\n";
	}

	function print_footer() {
		global $debug;
		if($debug) print "<!-- Starting print_footer() -->\n";
		print "\t\t\t\t</td>\n\t\t\t</tr>\n";
		print "\t\t</table>\n";
		print "\t</body>\n";
		print "</html>\n";
		if($debug) print "<!-- Ending print_footer() -->\n";
	}


?>
