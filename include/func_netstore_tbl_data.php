<?php

global $FUNC_NETSTORE_TBL_DATA_INC;
if (!$FUNC_NETSTORE_TBL_DATA_INC) {
	$FUNC_NETSTORE_TBL_DATA_INC=1;

	include($INC["mysql"]);

	class DATA {

		var $db, $resultid;
		var $table, $debug;
		var $con_info;
		var $startip;
		var $stopip;
		var $from_str, $to_str;
		var $consp;

		function DATA($con_info) {
			global $CONNECTION_SPEED;
			$this->consp = $CONNECTION_SPEED;
			$this->con_info = $con_info;
			$this->debug = $con_info["DEBUG"];
			if($this->debug) { error_reporting(E_ALL); }
			$this->db = new Database($con_info);
			$this->table = "data_raw";
			$this->db->connect();
			
			// ranges can be used for internal / external
			// or two internal ranges
			// This is simply for figuring out what your total uploading and downloading
			// bandwith is. The database stores all info this is just to format it to the
			// likings of your network.
			
			$range1_start = "10.0.0.0";
			$range1_stop  = "10.10.11.0";
			$range2_start = "66.112.37.224";
			$range2_stop  = "66.112.37.254";
	
			// The following code is use to figure out what is internal and
			// what is external to your network.
			
			// <DO NOT TOUCH> the following code	
			$this->startip = $this->ip2nilolong($range1_start);
			$this->stopip  = $this->ip2nilolong($range1_stop);
			$from_gt = "`from` >= $this->startip";
			$from_lt = "`from` <= $this->stopip";
			$to_gt = "`to` >= $this->startip";
			$to_lt = "`to` <= $this->stopip";
			$from_str1 = "($from_gt and $from_lt)";
			$to_str1 = "($to_gt and $to_lt)";
			$this->startip = $this->ip2nilolong($range2_start);
			$this->stopip  = $this->ip2nilolong($range2_stop);
			$from_gt = "`from` >= $this->startip";
			$from_lt = "`from` <= $this->stopip";
			$to_gt = "`to` >= $this->startip";
			$to_lt = "`to` <= $this->stopip";
			$from_str2 = "($from_gt and $from_lt)";
			$to_str2 = "($to_gt and $to_lt)";
			$this->from_str = "($from_str1 or $from_str2)";
			$this->to_str = "($to_str1 or $to_str2)";
			// </DO NOT TOUCH>

		}

		function nilolong2ip($long) {
			$a = floor($long / pow(2,24));
			$long -= ($a * pow(2,24));
			$b = floor($long / pow(2,16));
			$long -= ($b * pow(2,16));
			$c = floor($long / pow(2,8));
			$long -= ($c * pow(2,8));
			$d = $long;
			$ip = join(".", array($a, $b, $c, $d));
			return $ip;
		}

		function ip2nilolong($ip) {
			$array = split("\.", $ip);
			$a = pow(2, 24) * $array[0];
			$b = pow(2, 16) * $array[1];
			$c = pow(2, 8) * $array[2];
			$d = 1 * $array[3];
			$long = $a + $b + $c + $d;
			return $long;	
		}
		
		function graph($im, $hostid, $time, $table) {
			$w = imagesx($im);
			$h = imagesy($im);

			if($table == "data_raw") $step_size = 1;
			if($table == "data_seconds") $step_size = 1;
			if($table == "data_minutes") $step_size = 60;
			if($table == "data_hours") $step_size = 3600;
			if($table == "data_daily") $step_size = 86400;

			//$this->db->debug = 1;
			
			$maxcon = (($this->consp/8)*$step_size);

			$this->db->query("lock table $table read");

			$res = $this->db->query("select sum(amount) as sum, unix_timestamp(time) as time2 from $table where `from`=$hostid group by time order by time2 desc");
			while($ar = $this->db->get_array($res)) {
				if($this->debug) print $ar["time2"]."<BR>\n";
				$working["from"][$ar["time2"]] = $ar["sum"];
			}
			$res = $this->db->query("select sum(amount) as sum, unix_timestamp(time) as time2 from $table where `to`=$hostid group by time order by time2 desc");
			while($ar = $this->db->get_array($res)) {
				if($this->debug) print $ar["time2"]."<BR>\n";
				$working["to"][$ar["time2"]] = $ar["sum"];
			}

			if(isset($working)) {
			
				$min = $this->get_unix_time($hostid, $table, "min");
				$max = $this->get_unix_time($hostid, $table, "max");
				$old = ($working["from"][$max]/$maxcon * $h);

				$this->db->query("unlock tables");
	
				$c = 0;
			
				if($table == "data_raw") { $max = time(); }
				for($i = $max; ($i >= $min) && ($c < $time); $i-=$step_size, $c++) {
					if(isset($working["from"][$i])) {
						$hefr = ($working["from"][$i]/$maxcon * $h);
					} else $hefr = 0;
					if(isset($working["to"][$i])) {
						$heto = ($working["to"][$i]/$maxcon * $h);
					} else $heto = 0;
					
					for($j = 0; $j <= $heto; $j++) {
						imagesetpixel($im, $c, ($h - $j), 2);
					}
					imageline($im, $c, ($h - $hefr), $c - 1, ($h - $old), 1);
					$old = $hefr;
				}
			}
		}
		
		function get_unix_time($hostid, $table, $minmax) {
			$ar = $this->db->get_array($this->db->query("select $minmax(unix_timestamp(time)) as time from $table where `from`=$hostid"));
			return $ar["time"];	
		}
		
		function graph_host($im, $hostid, $time) {
			$w = imagesx($im);
			$h = imagesy($im);

			$array = $this->get_host_info($hostid, $time);
			$max = $this->consp/8;

			$old = ($array["to"][0]/$max * $h);

			for($i = 0; $i < count($array["to"]); $i++) {
				$heto = ($array["to"][$i]/$max * $h);
				$hefr = ($array["from"][$i]/$max * $h);

				for($j = 0; $j <= $heto; $j++) {
					imagesetpixel($im, $i, ($h - $j), 2);
				}
				imageline($im, $i, ($h - $hefr), $i - 1, ($h - $old), 1);
				$old = $hefr;
			}
		}

		function graph_minute($im, $hostid, $time) {
			$w = imagesx($im);
			$h = imagesy($im);

			$array = $this->get_host_info_minute($hostid, $time);
			$max = $this->consp/8;

			$old = ($array["to"][0]/$max * $h);

			for($i = 0; $i < count($array["to"]); $i++) {
				$heto = ($array["to"][$i]/$max * $h);
				$hefr = ($array["from"][$i]/$max * $h);

				for($j = 0; $j <= $heto; $j++) {
					imagesetpixel($im, $i, ($h - $j), 2);
				}
				imageline($im, $i, ($h - $hefr), $i - 1, ($h - $old), 1);
				$old = $hefr;
			}

		}

		function graph_all($im, $time) {
			$w = imagesx($im);
			$h = imagesy($im);

			$array = $this->get_info($time);
			$max = $this->consp/8;

			$old = ($array["to"][0]/$max * $h);

			for($i = 0; $i < count($array["to"]); $i++) {
				$heto = ($array["to"][$i]/$max * $h);
				$hefr = ($array["from"][$i]/$max * $h);

				for($j = 0; $j <= $heto; $j++) {
					imagesetpixel($im, $i, ($h - $j), 2);
				}
				imageline($im, $i, ($h - $hefr), $i - 1, ($h - $old), 1);
				$old = $hefr;
			}

		}

		function get_host_info($hostid, $time) {
			$array = $this->db->get_array($this->db->query("select UNIX_TIMESTAMP(now()) as start"));
			$start = $array["start"];

			for($i = 0; $i < $time; $i++) {
				$ret["from"][] = $this->get_host_sum_from($hostid, $start - $i);
				$ret["to"][] = $this->get_host_sum_to($hostid, $start - $i);
			}
			return $ret;
		}

		function get_host_info_minute($hostid, $time) {
			$array = $this->db->get_array($this->db->query("select UNIX_TIMESTAMP(now()) as start"));
			$start = $array["start"];

			for($i = 0; $i <= $time; $i++) {
				$ret["from"][] = $this->get_host_sum_from_minute($hostid, $start, $i);
				$ret["to"][] = $this->get_host_sum_to_minute($hostid, $start, $i);
			}
			return $ret;
		}

		function get_info($time) {
			$array = $this->db->get_array($this->db->query("select UNIX_TIMESTAMP(now()) as start"));
			$start = $array["start"];

			for($i = 0; $i < $time; $i++) {
				$ret["from"][] = $this->get_sum_from($start - $i);
				$ret["to"][] = $this->get_sum_to($start - $i);
			}
			return $ret;
		}

		function get_host_sum_from_minute($hostid, $time, $i) {
			$array = $this->db->get_array($this->db->query("select sum(amount)/60 as s from data_consol where time>FROM_UNIXTIME($time - (($i + 1) * 60)) and time <= FROM_UNIXTIME($time - ($i * 60)) and `from`=$hostid group by `from`"));
			return $array["s"];
		}

		function get_host_sum_from($hostid, $time) {
			$array = $this->db->get_array($this->db->query("select sum(amount) as s from $this->table where time=FROM_UNIXTIME($time) and `from`=$hostid group by `from`"));
			return $array["s"];
		}

		function get_sum_from($time) {
			$array = $this->db->get_array($this->db->query("select sum(amount) as s from $this->table where $this->from_str and time=FROM_UNIXTIME($time)"));
			return $array["s"];
		}

		function get_host_sum_to_minute($hostid, $time, $i) {
			$array = $this->db->get_array($this->db->query("select sum(amount)/60 as s from data_consol where time>FROM_UNIXTIME($time - (($i + 1) * 60)) and time <= FROM_UNIXTIME($time - ($i * 60)) and `to`=$hostid group by `to`"));
			#$array = $this->db->get_array($this->db->query("select sum(amount) as s from $this->table where time=FROM_UNIXTIME($time) and `to`=$hostid group by `to`"));
			return $array["s"];
		}

		function get_host_sum_to($hostid, $time) {
			$array = $this->db->get_array($this->db->query("select sum(amount) as s from $this->table where time=FROM_UNIXTIME($time) and `to`=$hostid group by `to`"));
			return $array["s"];
		}

		function get_sum_to($time) {
			$array = $this->db->get_array($this->db->query("select sum(amount) as s from $this->table where $this->to_str and time=FROM_UNIXTIME($time)"));
			return $array["s"];
		}

		function print_seconds_breakdown($number, $time) {
			$res = $this->db->query("select `from`, `to` as too, sum(amount)/$time/1024 as s from $this->table where time > ((now()- interval $time second)+0) group by `from`, `to` order by s desc limit 0,$number");
			print "<table border=1>
							<tr>
								<td>Uploader</td>
								<td> --> </td>
								<td>Downloader</td>
								<td>Rate*</td>
							</tr>";

			while(($array = $this->db->get_array($res))) {
				print "<tr><td><a href=\"HostInfo.php?hostid=$array[from]\">".$this->nilolong2ip($array["from"])."</a></td><td>&nbsp;</td>";
				print "<td><a href=\"HostInfo.php?hostid=$array[too]\">".$this->nilolong2ip($array["too"])."</a></td>";
				print "<td align=right>".number_format($array["s"], 2)."</td></tr>";
			}
			print "<tr><td align=left colspan=4>* Rates are in KB/s</td></tr></table>";

		} // end function print_top

		function print_top($number, $time, $dir) {

			if($dir == "up") {
				$ar = "from";
				$ip_str = $this->from_str;
				$name = "Uploaders";
			} else if($dir == "down") {
				$ar = "to";
				$ip_str = $this->to_str;
				$name = "Downloaders";
			} else {
				return;
			}

			$q = "select `$ar`, sum(amount)/$time/1024 as s from $this->table where time > ((now()- interval $time second)+0) and $ip_str group by `$ar` order by s desc limit 0,$number";

			$res = $this->db->query($q);

			print "<table border=1>
							<tr>
								<td>$name</td>
								<td>Rate*</td>
							</tr>";

			while($array = $this->db->get_array($res)) {
				print "<tr>";
				print "<td><a href=\"HostInfo.php?hostid=$array[$ar]\">".$this->nilolong2ip($array[$ar])."</a></td>";
				print "<td align=right>".number_format($array["s"], 2)."</td></tr>";
				print "</tr>";
			}
			$array = $this->db->get_array($this->db->query("select sum(amount)/$time/1024 as s from $this->table where $ip_str and time > ((now()- interval $time second)+0)"));
			print "<tr><td>Total</td><td><a href=\"host.php?hostid=0\">".number_format($array["s"], 2)."</a></td></tr>";
			print "</table>";

		}

	} // end class DATA

} // end include protection

?>
