<?php

global $FUNC_DB_MYSQL_INC;

if (!$FUNC_DB_MYSQL_INC) {
	$FUNC_DB_MYSQL_INC=1;

	class Database {
		var $connection_id, $debug;
		var $dbinfo;

		function Database($con_info, $debug=0) {
			if($con_info["DEBUG"]) $this->debug = $con_info["DEBUG"];
			$this->dbinfo = $con_info;
			if($this->debug) print "The debug level is $this->debug in func_db_mysql.inc<BR>\n";
		}

		function set_auth_info($host, $user, $pass, $name) {
			$this->dbinfo["DBHOST"] = $host;
			$this->dbinfo["DBUSER"] = $user;
			$this->dbinfo["DBPASS"] = $pass;
			$this->dbinfo["DBNAME"] = $name;
		}

		function connect() {
			if($this->debug) $this->connection_id = mysql_connect($this->dbinfo["DBHOST"], $this->dbinfo["DBUSER"], $this->dbinfo["DBPASS"]);
			else $this->connection_id = @mysql_connect($this->dbinfo["DBHOST"], $this->dbinfo["DBUSER"], $this->dbinfo["DBPASS"]);
			if($this->debug > 1) {
				if($this->debug > 2) { $pass = $this->dbinfo["DBPASS"]; }
				else $pass = "********";
				print "We are connecting to ".$this->dbinfo["DBHOST"]." with User:".$this->dbinfo["DBUSER"]." Password:$pass<BR>\n";
			}
			if($this->connection_id) {
				if($this->dbinfo["DBNAME"]) {
					//if($this->debug) print "Using default database: ".$this->dbinfo["DBNAME"]."<BR>\n";
					if($this->usedatabase()) {
						return $this->connection_id;
					} else {
						return 0;
					}
				} else {
					return $this->connection_id;
				}
			}
			else {
				if($this->debug) print "Error in class Database: Can't connect to database<BR>\n";
				$body = "\n\nCannot connect to MySQL server on ".$this->dbinfo["DBHOST"]."\n\n";
				$this->errors("MySQL down", $body);
				return 0;
			}
		}

		function errors($subject,$data) {
			global $ADMINEMAIL1, $ERROREMAIL;
			if($ADMINEMAIL1 && $ERROREMAIL) {
				mail($ADMINEMAIL1, $subject, $data, "From: $ERROREMAIL\nContent-Type: text/html; charset=iso-8859-1");
			} else {
				if($this->debug) {
					print "Error email not setup please set \$ADMINEMAIL1 and \$ERROREMAIL to get an email desribing this error.<BR>\n";
				}
			}
		}

		function usedatabase($dbname = '') {
			if($this->debug) { print "userdatebase(): This is the database we are trying to use: ".$this->dbinfo["DBNAME"]."<BR>\n"; }
			if($dbname) $this->dbinfo["DBNAME"] = $dbname;
			if($this->debug) $result_id = mysql_select_db($this->dbinfo["DBNAME"], $this->connection_id);
			else $result_id = @mysql_select_db($this->dbinfo["DBNAME"], $this->connection_id);
			if($result_id) return $result_id;
			else {
				if($this->debug) {
					print "Error in class Database: Can't select database<BR>\n";
					print "This is the database we are trying to use: $this->dbinfo[DBNAME]<BR>\n";
				} return 0;
			}
		}

		function query($querystring) {
			if($this->debug) print "This is the query: $querystring<BR>\n";
			if(!$querystring) {
				if($this->debug) print "There was no query passed to the query function in class Database<BR>\n";
				return 0;
			}
			if($this->debug) $result_id = mysql_query($querystring, $this->connection_id);
			else $result_id = @mysql_query($querystring, $this->connection_id);
			if($result_id) {
				if($this->debug) print "Query was good: $result_id<BR>\n";
				return $result_id;
			}
			else {
				if($this->debug) {
					print "Error in class Database -> query()<BR>\n";
					print "No result_id.<BR>\n";
					print $this->get_error()."<BR>\n";
				} return 0;
			}
		}

		function num_rows($resultid) { return mysql_num_rows($resultid); }
		function get_error() { return mysql_error($this->connection_id); }
		function get_id() { return mysql_insert_id($this->connection_id); }
		function get_row($resultid) { return mysql_fetch_row($resultid); }
		function free($resultid) { return mysql_free_result($resultid); }
		function get_field($resultid) { return mysql_fetch_field($resultid); }
		function get_array($resultid) { return mysql_fetch_array($resultid); }
		function get_object($resultid) { return mysql_fetch_object($resultid); }
		function get_assarray($resultid) { return mysql_fetch_array($resultid, MYSQL_ASSOC); }
		function get_enuarray($resultid) { return mysql_fetch_array($resultid, MYSQL_NUM); }

	}
}

?>
