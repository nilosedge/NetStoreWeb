<?php
 
	include("config.php");
	include($INC["netstore"]);
	
	$data = new Data($MYSQL_CONNECTION_INFO["netstore"]);
	if(isset($_GET["hostid"])) $host = $_GET["hostid"];
	if(isset($_GET["table"])) $table = $_GET["table"];
	if(isset($_GET["width"])) $width = $_GET["width"];
	
	header("Content-type: image/png");
	$im = imagecreate($width, $NETSTORE_IMAGE_HEIGHT) or die("Cannot Initialize new GD image stream");
	$background_color = imagecolorallocate($im, 255, 255, 255);
	$blue = imagecolorallocate($im, 0, 0, 255);
	$green = imagecolorallocate($im, 0, 255, 0);
	$data->graph($im, $host, $width, "data_$table");
	$ip = $data->nilolong2ip($host);
	$text_color = imagecolorallocate($im, 233, 14, 91);
	imagestring($im, 1, 5, 5,  $ip, $text_color);
	imagepng($im);
	imagedestroy($im);
	
?>
