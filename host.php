<?php

	include("config.php");
	include($INC["netstore"]);

	$data = new Data($MYSQL_CONNECTION_INFO["netstore"]);
	if(isset($_GET["hostid"])) $host = $_GET["hostid"];

	header("Content-type: image/png");

	if(isset($host) && $host != 0) {
		$im = imagecreate($NETSTORE_IMAGE_WIDTH, $NETSTORE_IMAGE_HEIGHT) or die("Cannot Initialize new GD image stream");
		$background_color = imagecolorallocate($im, 255, 255, 255);
		$blue = imagecolorallocate($im, 0, 0, 255);
		$green = imagecolorallocate($im, 0, 255, 0);
		//$data->graph_minute($im, $host, $NETSTORE_IMAGE_WIDTH);
		$data->graph_host($im, $host, $NETSTORE_IMAGE_WIDTH);
		$ip = $data->nilolong2ip($host);
		$text_color = imagecolorallocate($im, 233, 14, 91);
		imagestring($im, 1, 5, 5,  $ip, $text_color);
	} else if(isset($host) && $host == 0) {
		$im = imagecreate($NETSTORE_IMAGE_WIDTH, $NETSTORE_IMAGE_HEIGHT) or die("Cannot Initialize new GD image stream");
		$background_color = imagecolorallocate($im, 255, 255, 255);
		$blue = imagecolorallocate($im, 0, 0, 255);
		$green = imagecolorallocate($im, 0, 255, 0);
		$data->graph_all($im, $NETSTORE_IMAGE_WIDTH);
		$text_color = imagecolorallocate($im, 233, 14, 91);
		imagestring($im, 1, 5, 5,  "All Network Stats", $text_color);
	} else {
		$im = imagecreate($NETSTORE_IMAGE_WIDTH/8, $NETSTORE_IMAGE_HEIGHT) or die("Cannot Initialize new GD image stream");
		$background_color = imagecolorallocate($im, 255, 255, 255);
		$text_color = imagecolorallocate($im, 233, 14, 91);
		imagestring($im, 1, 5, 5, "No Data Found", $text_color);
	}

	imagepng($im);
	imagedestroy($im);

?>
