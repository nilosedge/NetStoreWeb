<?php

	include("config.php");
	include($INC["netstore"]);

	$data = new DATA($MYSQL_CONNECTION_INFO["netstore"]);

	print "<html><head><title>Network Usage</title><meta http-equiv=\"REFRESH\" content=\"$REFRESH_RATE; URL=\"".$_SERVER["PHP_SELF"]."\"></head>";

	print "<table border=0>";
	print "<tr><td>";
	print "<BR>Raw Network Averages for the last $STATS_TIME seconds<BR>\n";
	print "<table><tr><td valign=top>";
	$data->print_top($TOP_STATS, $STATS_TIME, "up");
	print "</td><td valign=top>";
	$data->print_top($TOP_STATS, $STATS_TIME, "down");
	print "</tr></table>";
	print "</td></tr>";
	print "<tr><td>";
	print "<BR><BR>Raw Network Averages for the last $STATS_TIME seconds<BR>\n";
	$data->print_seconds_breakdown($TOP_RAW_STATS, $STATS_TIME);
	print "</td></tr>";
	print "</table>";

?>
